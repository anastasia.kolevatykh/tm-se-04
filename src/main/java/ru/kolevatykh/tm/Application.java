package ru.kolevatykh.tm;

import ru.kolevatykh.tm.bootstrap.Bootstrap;

public class Application {
    public static void main(String[] args) {
        Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}
