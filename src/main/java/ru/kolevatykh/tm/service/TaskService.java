package ru.kolevatykh.tm.service;

import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.repository.TaskRepository;

import java.util.Date;
import java.util.List;

public class TaskService {
    private TaskRepository taskRepository;

    public TaskService(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    public void create(String name, String description, Date startDate, Date endDate) {
        if (name == null || name.isEmpty()) return;
        taskRepository.persist(new Task(name, description, startDate, endDate));
    }

    public List<Task> showAll() {
        List<Task> taskList = taskRepository.findAll();
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    public List<Task> showProjectTasks(String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        List<Task> taskList = taskRepository.findTasksByProjectId(projectId);
        if (taskList.isEmpty()) return null;
        return taskList;
    }

    public Task findOneByName(String name) {
        if (name == null || name.isEmpty()) return null;
        return taskRepository.findOneByName(name);
    }

    public void assignToProject(String id, String projectId) {
        if (id == null || id.isEmpty() || projectId == null || projectId.isEmpty()) return;
        taskRepository.assignToProject(id, projectId);
    }

    public void update(String id, String name, String description, Date startDate, Date endDate) {
        if (name == null || name.isEmpty()) return;

        Task task = taskRepository.findOneById(id);

        if (task != null) {
            task.setName(name);
            task.setDescription(description);
            task.setStartDate(startDate);
            task.setEndDate(endDate);
            taskRepository.merge(task);
        } else {
            taskRepository.persist(new Task(name, description, startDate, endDate));
        }
    }

    public void clearAll() {
        taskRepository.removeAll();
    }

    public void clearTasksWithProjectId() {
        taskRepository.removeTasksWithProjectId();
    }

    public void remove(String id) {
        if (id == null || id.isEmpty()) return;
        taskRepository.remove(id);
    }

    public void removeProjectTasks(String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeProjectTasks(projectId);
    }
}
