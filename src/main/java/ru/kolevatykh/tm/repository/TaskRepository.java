package ru.kolevatykh.tm.repository;

import ru.kolevatykh.tm.entity.Task;

import java.util.*;

public class TaskRepository {
    private Map<String, Task> taskMap;

    public TaskRepository() {
        this.taskMap = new HashMap<>();
    }

    public List<Task> findAll() {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            taskList.add(entry.getValue());
        }
        return taskList;
    }

    public Task findOneById(String id) {
        return taskMap.get(id);
    }

    public Task findOneByName(String name) {
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getName().equals(name))
                return entry.getValue();
        }
        return null;
    }

    public void persist(Task task) {
        taskMap.put(task.getId(), task);
    }

    public void merge(Task task) {
        taskMap.put(task.getId(), task);
    }

    public void remove(String id) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getId().equals(id))
                it.remove();
        }
    }

    public void removeAll() {
        taskMap = new HashMap<>();
    }

    public void removeTasksWithProjectId() {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getProjectId() != null)
                it.remove();
        }
    }

    public void removeProjectTasks(String projectId) {
        for (Iterator<Map.Entry<String, Task>> it = taskMap.entrySet().iterator(); it.hasNext(); ) {
            Map.Entry<String, Task> entry = it.next();
            Task task = entry.getValue();
            if (task.getProjectId().equals(projectId))
                it.remove();
        }
    }

    public List<Task> findTasksByProjectId(String projectId) {
        List<Task> taskList = new ArrayList<>();
        for (Map.Entry<String, Task> entry : taskMap.entrySet()) {
            if (entry.getValue().getProjectId().equals(projectId))
                taskList.add(entry.getValue());
        }
        return taskList;
    }

    public void assignToProject(String id, String projectId) {
        taskMap.get(id).setProjectId(projectId);
    }
}
