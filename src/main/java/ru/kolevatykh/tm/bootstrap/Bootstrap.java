package ru.kolevatykh.tm.bootstrap;

import ru.kolevatykh.tm.command.TerminalCommand;
import ru.kolevatykh.tm.entity.Project;
import ru.kolevatykh.tm.entity.Task;
import ru.kolevatykh.tm.repository.ProjectRepository;
import ru.kolevatykh.tm.repository.TaskRepository;
import ru.kolevatykh.tm.service.ProjectService;
import ru.kolevatykh.tm.service.TaskService;
import ru.kolevatykh.tm.util.DateFormatter;

import java.util.Date;
import java.util.List;
import java.util.Scanner;

public class Bootstrap {
    private Scanner scanner = new Scanner(System.in);
    private ProjectRepository projectRepository = new ProjectRepository();
    private TaskRepository taskRepository = new TaskRepository();
    private ProjectService projectService = new ProjectService(projectRepository);
    private TaskService taskService = new TaskService(taskRepository);

    private void showCommands() {
        String help = TerminalCommand.HELP + ": \t\tShow all commands.\n"
                + TerminalCommand.PROJECT_CREATE + ": Create new project.\n"
                + TerminalCommand.PROJECT_LIST + ": \tShow all projects.\n"
                + TerminalCommand.PROJECT_SHOW + ": \tShow tasks of selected project.\n"
                + TerminalCommand.PROJECT_UPDATE + ": Update selected project.\n"
                + TerminalCommand.PROJECT_CLEAR + ": \tRemove all projects.\n"
                + TerminalCommand.PROJECT_REMOVE + ": Remove selected project.\n"
                + TerminalCommand.TASK_CREATE + ": \tCreate new task.\n"
                + TerminalCommand.TASK_LIST + ": \tShow all tasks.\n"
                + TerminalCommand.TASK_ASSIGN + ": \tAssign task to project.\n"
                + TerminalCommand.TASK_UPDATE + ": \tUpdate selected task.\n"
                + TerminalCommand.TASK_CLEAR + ": \tRemove all tasks.\n"
                + TerminalCommand.TASK_REMOVE + ": \tRemove selected task.\n"
                + TerminalCommand.EXIT + ": \t\tExit.";
        System.out.println(help);
    }

    private void createProject() {
        System.out.println("[PROJECT CREATE]\nEnter project name: ");
        String projectName = scanner.nextLine();

        System.out.println("Enter project description: ");
        String projectDescription = scanner.nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = scanner.next();
        Date startDate = DateFormatter.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = scanner.next();
        Date endDate = DateFormatter.parseDate(projectEndDate);

        projectService.create(projectName, projectDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void showProjects() {
        System.out.println("[PROJECT LIST]");
        List<Project> projectList = projectService.showAll();

        if (projectList == null) {
            System.out.println("No projects yet.");
        } else {
            StringBuilder projects = new StringBuilder();
            int i = 0;

            for (Project project : projectList) {
                projects
                        .append(++i)
                        .append(". ")
                        .append(project.toString())
                        .append(System.lineSeparator());
            }

            String projectString = projects.toString();
            System.out.println(projectString);
        }
    }

    private void showProjectTasks() {
        System.out.println("[PROJECT SHOW]");
        String projectId = findProjectIdByName();

        List<Task> taskList = taskService.showProjectTasks(projectId);

        if (projectId != null && taskList != null) {
            System.out.println("[TASK LIST]");

            StringBuilder projectTasks = new StringBuilder();
            int i = 0;

            for (Task task : taskList) {
                if (projectId.equals(task.getProjectId())) {
                    projectTasks
                            .append(++i)
                            .append(". ")
                            .append(task.toString())
                            .append(System.lineSeparator());
                }
            }

            String taskString = projectTasks.toString();
            System.out.println(taskString);
        } else {
            System.out.println("No tasks yet.");
        }
    }

    private void updateProject() {
        System.out.println("[PROJECT UPDATE]");
        String projectId = findProjectIdByName();

        System.out.println("Enter new project name: ");
        String projectName = scanner.nextLine();

        System.out.println("Enter new project description: ");
        String projectDescription = scanner.nextLine();

        System.out.println("Enter project start date: ");
        String projectStartDate = scanner.next();
        Date startDate = DateFormatter.parseDate(projectStartDate);

        System.out.println("Enter project end date: ");
        String projectEndDate = scanner.next();
        Date endDate = DateFormatter.parseDate(projectEndDate);

        projectService.update(projectId, projectName, projectDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void clearProject() {
        System.out.println("[PROJECT CLEAR]");
        taskService.clearTasksWithProjectId();
        projectService.clearAll();
        System.out.println("[Removed all projects with tasks.]\n[OK]");
    }

    private void removeProject() {
        System.out.println("[PROJECT REMOVE]");
        String projectId = findProjectIdByName();

        if (projectId != null) {
            taskService.removeProjectTasks(projectId);
            projectService.remove(projectId);
            System.out.println("[Removed project with tasks.]\n[OK]");
        }
    }

    private String findProjectIdByName() {
        System.out.println("Enter project name: ");
        String projectName = scanner.nextLine();

        if (projectService.findByName(projectName) == null) {
            System.out.println("[The project '" + projectName + "' does not exist!]");
            return null;
        }
        return projectService.findByName(projectName).getId();
    }

    private void createTask() {
        System.out.println("[TASK CREATE]\nEnter task name: ");
        String taskName = scanner.nextLine();

        System.out.println("Enter task description: ");
        String taskDescription = scanner.nextLine();

        System.out.println("Enter task start date: ");
        String taskStartDate = scanner.next();
        Date startDate = DateFormatter.parseDate(taskStartDate);

        System.out.println("Enter task end date: ");
        String taskEndDate = scanner.next();
        Date endDate = DateFormatter.parseDate(taskEndDate);

        taskService.create(taskName, taskDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> taskList = taskService.showAll();

        if (taskList == null) {
            System.out.println("No tasks yet.");
        } else {
            StringBuilder tasks = new StringBuilder();
            int i = 0;

            for (Task task : taskList) {
                tasks
                        .append(++i)
                        .append(". ")
                        .append(task.toString())
                        .append(System.lineSeparator());
            }

            String taskString = tasks.toString();
            System.out.println(taskString);
        }
    }

    private void assignTaskToProject() {
        System.out.println("[ASSIGN TASK TO PROJECT]");
        String taskId = findTaskIdByName();
        String projectId = findProjectIdByName();

        if (projectId != null && taskId != null) {
            taskService.assignToProject(taskId, projectId);
            System.out.println("[OK]");
        } else {
            System.out.println("[Error]  taskId: " + taskId + " projectId: " + projectId);
        }
    }

    private void updateTask() {
        System.out.println("[TASK UPDATE]");
        String taskId = findTaskIdByName();

        System.out.println("Enter new task name: ");
        String taskName = scanner.nextLine();

        System.out.println("Enter new task description: ");
        String taskDescription = scanner.nextLine();

        System.out.println("Enter new task start date: ");
        String taskStartDate = scanner.next();
        Date startDate = DateFormatter.parseDate(taskStartDate);

        System.out.println("Enter new task end date: ");
        String taskEndDate = scanner.next();
        Date endDate = DateFormatter.parseDate(taskEndDate);

        taskService.update(taskId, taskName, taskDescription, startDate, endDate);
        System.out.println("[OK]");
    }

    private void clearTasks() {
        System.out.println("[TASK CLEAR]");
        taskService.clearAll();
        System.out.println("[Removed all tasks.]");
        System.out.println("[OK]");
    }

    private void removeTask() {
        System.out.println("[TASK REMOVE]");
        String taskId = findTaskIdByName();

        if (taskId != null) {
            taskService.remove(taskId);
            System.out.println("[OK]");
        }
    }

    private String findTaskIdByName() {
        System.out.println("Enter task name: ");
        String taskName = scanner.nextLine();

        if (taskService.findOneByName(taskName) == null) {
            System.out.println("[The task '" + taskName + "' does not exist!]");
            return null;
        }

        return taskService.findOneByName(taskName).getId();
    }

    public void init() {
        System.out.println("*** Welcome to task manager ***"
                + "\nType \"help\" for details.");

        String input = scanner.nextLine();

        while (input != null) {
            input = scanner.nextLine();
            switch (input) {
                case TerminalCommand.HELP:
                case TerminalCommand.H:
                    showCommands();
                    break;
                case TerminalCommand.PROJECT_CREATE:
                case TerminalCommand.PCR:
                    createProject();
                    break;
                case TerminalCommand.PROJECT_LIST:
                case TerminalCommand.PL:
                    showProjects();
                    break;
                case TerminalCommand.PROJECT_SHOW:
                case TerminalCommand.PSH:
                    showProjectTasks();
                    break;
                case TerminalCommand.PROJECT_UPDATE:
                case TerminalCommand.PU:
                    updateProject();
                    break;
                case TerminalCommand.PROJECT_REMOVE:
                case TerminalCommand.PR:
                    removeProject();
                    break;
                case TerminalCommand.PROJECT_CLEAR:
                case TerminalCommand.PCL:
                    clearProject();
                    break;
                case TerminalCommand.TASK_CREATE:
                case TerminalCommand.TCR:
                    createTask();
                    break;
                case TerminalCommand.TASK_LIST:
                case TerminalCommand.TL:
                    showTasks();
                    break;
                case TerminalCommand.TASK_ASSIGN:
                case TerminalCommand.TA:
                    assignTaskToProject();
                    break;
                case TerminalCommand.TASK_UPDATE:
                case TerminalCommand.TU:
                    updateTask();
                    break;
                case TerminalCommand.TASK_REMOVE:
                case TerminalCommand.TR:
                    removeTask();
                    break;
                case TerminalCommand.TASK_CLEAR:
                case TerminalCommand.TCL:
                    clearTasks();
                    break;
                case TerminalCommand.EXIT:
                case TerminalCommand.E:
                    System.exit(0);
                    break;
            }
        }
    }
}
